package adapter_test

import (
	"gitlab.com/jgunnink/ogserver/adapter"
	"gitlab.com/jgunnink/ogserver/ports"
)

// Checks that our adapter correclty implements the port interface
// Will be a compile time error if the interface is incorrect
var _scraperImplementsInterface ports.Scraper = adapter.Scraper{}

// Integration Tests goes here
