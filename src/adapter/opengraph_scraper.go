package adapter

import "github.com/otiai10/opengraph/v2"

type Scraper struct{}

func New() Scraper {
	return Scraper{}
}

func (_ Scraper) Scrape(url string) (*opengraph.OpenGraph, error) {
	ogp, err := opengraph.Fetch(url)
	if err != nil {
		return nil, err
	}

	return ogp, nil
}
