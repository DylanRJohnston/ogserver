package ports

import "github.com/otiai10/opengraph/v2"

type Scraper interface {
	Scrape(requestUrl string) (*opengraph.OpenGraph, error)
}
