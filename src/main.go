package main

import (
	"net/http"

	"gitlab.com/jgunnink/ogserver/adapter"
	"gitlab.com/jgunnink/ogserver/usecase/server"
)

func main() {
	scraper := adapter.New()
	server := server.New(scraper)

	http.ListenAndServe(":8080", server.HandlerFunc())
}
