module gitlab.com/jgunnink/ogserver

go 1.16

require github.com/otiai10/opengraph/v2 v2.1.0

require golang.org/x/net v0.0.0-20211216030914-fe4d6282115f // indirect
