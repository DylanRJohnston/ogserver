package server

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/jgunnink/ogserver/ports"
)

type Server struct {
	scraper ports.Scraper
}

type RequestUrl struct {
	Url string `json:"url"`
}

func New(scraper ports.Scraper) *Server {
	return &Server{scraper}
}

func (s *Server) HandlerFunc() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req RequestUrl

		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil || req.Url == "" {
			http.Error(w, fmt.Sprintf("could not decode user payload: %v", err), http.StatusBadRequest)
			return
		}

		response, err := s.scraper.Scrape(req.Url)
		if err != nil {
			http.Error(w, fmt.Sprintf("Error in processing scrape. Err: %v", err), http.StatusInternalServerError)
		}

		jsonResp, err := json.Marshal(response)
		if err != nil {
			http.Error(w, fmt.Sprintf("Error in marshalling JSON. Err: %v", err), http.StatusInternalServerError)
			return
		}

		w.Write(jsonResp)
	}
}
