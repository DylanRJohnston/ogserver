package server_test

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/otiai10/opengraph/v2"
	"gitlab.com/jgunnink/ogserver/usecase/server"
)

type MockScraperService struct {
	ScraperFunc func(requestUrl string) (*opengraph.OpenGraph, error)
}

func (m *MockScraperService) Scrape(requestUrl string) (*opengraph.OpenGraph, error) {
	return m.ScraperFunc(requestUrl)
}

var returnValue *opengraph.OpenGraph = &opengraph.OpenGraph{
	URL:         "http://example.com",
	Title:       "Example Domain",
	Description: "This is an example Domain",
}

func TestOgScraper(t *testing.T) {
	t.Run("when a URL is provided", func(t *testing.T) {
		request := map[string]string{"url": "http://example.com"}
		json, _ := json.Marshal(request)

		service := &MockScraperService{
			ScraperFunc: func(requestUrl string) (*opengraph.OpenGraph, error) {
				return returnValue, nil
			},
		}
		server := server.New(service)

		req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(json))
		res := httptest.NewRecorder()

		server.HandlerFunc()(res, req)

		if res.Code != http.StatusOK {
			t.Errorf("Expected status code %d, got %d", http.StatusOK, res.Code)
		}

		expectedResponse := `{"title":"Example Domain","type":"","image":null,"url":"http://example.com","audio":null,"description":"This is an example Domain","determiner":"","locale":"","locale_alternate":null,"site_name":"","video":null,"favicon":{"url":""}}`
		if res.Body.String() != expectedResponse {
			t.Errorf("expected body of %q but got %q", res.Body.String(), expectedResponse)
		}
	})

	t.Run("when a URL not provided", func(t *testing.T) {
		request := map[string]string{"url": ""}
		json, _ := json.Marshal(request)

		service := &MockScraperService{
			ScraperFunc: func(requestUrl string) (*opengraph.OpenGraph, error) {
				return nil, nil
			},
		}
		server := server.New(service)

		req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(json))
		res := httptest.NewRecorder()

		server.HandlerFunc()(res, req)

		if res.Code != http.StatusBadRequest {
			t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, res.Code)
		}

		if res.Body.String() != "could not decode user payload: <nil>\n" {
			t.Errorf("expected body of %q but got %q", res.Body.String(), "could not decode user payload: <nil>\n")
		}
	})

	t.Run("when the response from the scraper is an error", func(t *testing.T) {
		request := map[string]string{"url": "http://example.com"}
		json, _ := json.Marshal(request)

		service := &MockScraperService{
			ScraperFunc: func(requestUrl string) (*opengraph.OpenGraph, error) {
				return nil, errors.New("Scraper failed")
			},
		}
		server := server.New(service)

		req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(json))
		res := httptest.NewRecorder()

		server.HandlerFunc()(res, req)

		if res.Code != http.StatusInternalServerError {
			t.Errorf("Expected status code %d, got %d", http.StatusInternalServerError, res.Code)
		}

		if res.Body.String() != "Error in processing scrape. Err: Scraper failed\nnull" {
			t.Errorf("expected body of %q but got %q", res.Body.String(), "Error in processing scrape. Err: Scraper failed\n")
		}
	})

	t.Run("when the response from the scraper cannot be marshalled to JSON", func(t *testing.T) {
		t.SkipNow() // TODO: unsure how to return a value from the scraper that would cause the marshalling to fail.
		request := map[string]string{"url": "http://example.com"}
		json, _ := json.Marshal(request)

		service := &MockScraperService{
			ScraperFunc: func(requestUrl string) (*opengraph.OpenGraph, error) {
				return returnValue, nil
			},
		}
		server := server.New(service)

		req := httptest.NewRequest(http.MethodPost, "/", bytes.NewBuffer(json))
		res := httptest.NewRecorder()

		server.HandlerFunc()(res, req)

		if res.Code != http.StatusInternalServerError {
			t.Errorf("Expected status code %d, got %d", http.StatusInternalServerError, res.Code)
		}

		if res.Body.String() != "Error in marshalling JSON. Err: \nnull" {
			t.Errorf("expected body of %q but got %q", "Error in marshalling JSON. Err: \n", res.Body.String())
		}
	})
}
